This package is maintained with git-buildpackage(1). It follows DEP-14 for
branch naming (e.g. using debian/master for the current version in Debian
unstable due Debian Python team policy).

It uses pristine-tar(1) to store enough information in git to generate bit
identical tarballs when building the package without having downloaded an
upstream tarball first.

When working with patches it is recommended to use "gbp pq import" to import
the patches, modify the source and then use "gbp pq export --commit" to commit
the modifications.

The changelog is generated using "gbp dch" so if you submit any changes don't
bother to add changelog entries but rather provide a nice git commit message
that can then end up in the changelog.

It is recommended to build the package with pbuilder using:

    gbp buildpackage --git-pbuilder

For information on how to set up a pbuilder environment see the git-pbuilder(1)
manpage. In short:

    DIST=sid git-pbuilder create
    gbp clone https://salsa.debian.org/python-team/packages/flask-security.git
    cd flask-security
    gbp buildpackage --git-pbuilder


Issue within the -doc package
=============================

The package python-flask-security-doc is shipping a file _static/openapi_view.html
in /usr/share/doc/python-flask-security-doc/html/.
This HTML file is loading a file 'openapi.yaml', but this is failing in the -doc
package due CORS (Cross-Origin-Resource-Sharing) restrictions.

For the etchnical background:
https://en.wikipedia.org/wiki/Cross-origin_resource_sharing

We have no option to prevent user from seeing this CORS error message. User
should navigate to the upstream resource of the documentation instead, as here
the data is provided by a webserver which is allowing to access files which are
available under the same domain the full data of this file is visible.

https://flask-security.readthedocs.io/en/stable/_static/openapi_view.html

We could and should of course provide a message by the HTML file to make it
easier for users to access the upstream data.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 24 Sep 2024 20:05:00 +0200
